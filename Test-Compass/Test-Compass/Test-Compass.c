/*
 * Test_Compass.c
 *
 * Created: 6/06/15 10:42:51 AM
 * Author: Thomas
 */ 

#include <avr/io.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <avr/pgmspace.h>

#define F_CPU 14745600UL
#include <util/delay.h>

#include "MPU-9150.h"
#include "Vector.h"

#define P_SAMPLE_MS	200

/* SERIAL OUTPUT DECLARATIONS & DEFINITIONS */
static int uart_putchar(char c, FILE *stream);
static int uart_getchar(FILE *stream);

FILE mystdout = FDEV_SETUP_STREAM(uart_putchar, NULL, _FDEV_SETUP_WRITE);
FILE mystdin = FDEV_SETUP_STREAM(NULL, uart_getchar, _FDEV_SETUP_READ);

static int uart_putchar(char c, FILE *stream)
{
	loop_until_bit_is_set(UCSR0A, UDRE0);
	UDR0 = c;
	return 0;
}

static int uart_getchar(FILE *stream)
{
	loop_until_bit_is_set(UCSR0A, RXC0); /* Wait until data exists. */
	return UDR0;
}

void init_uart(void)
{
	UCSR0B = (1<<RXEN0) | (1<<TXEN0);
	UBRR0 = 7;
	stdout = &mystdout;
	stdin = &mystdin;
}

uint8_t GetGyroBias(uint16_t, vector3 *);
uint8_t GetMagBias(uint16_t, vector3 *);

/* MAIN FUNCTION */
int main(void)
{
	// Define compass data variables
	vector3 accelData, gyroData, cmpData, gyroBias, cmpBias, angle;
	// Track number of lines printed
	uint16_t lineCount = 0;
	
	int32_t heading = 0;
	
	// Initialize serial connection
	init_uart();
	
	// Initialize MPU 9150 sensor
	printf("Initializing sensor test...\n");
	if (!MPUInit(F_CPU, MPU9150_ACCEL_RANGE_2G, MPU9150_GYRO_RANGE_500DPS)) {
		printf("Error initializing MPU-9150.\n");
		return 1;
	}
	
	printf("Getting gyro bias...\n");
	if (!GetGyroBias(200, &gyroBias))
		printf("Gyro calibration failed!\n");
	else
		printf("Bias: x = %ld, y = %ld, z = %ld\n", gyroBias.x, gyroBias.y, gyroBias.z);
	
	printf("Getting compass bias...\n");
	printf("On go, rotate device in all axes for 1 minute...\n");
	_delay_ms(2000);
	printf("Ready...\n");
	_delay_ms(2000);
	printf("Set...\n");
	_delay_ms(2000);
	printf("GO!\n");
	if (!GetMagBias(60*5, &cmpBias))
		printf("Compass calibration failed!\n");
	else
		printf("Bias: x = %ld, y = %ld, z = %ld\n", cmpBias.x, cmpBias.y, cmpBias.z);
	
	
	/* Loop forever, getting sensor data */
    while(1) {
		/* Print the header every 20 lines */
		if (lineCount++ == 0) {
			printf("    Accelerometer    #      Gyroscope      #       Compass\n");
			printf("    x     y     z    #    x     y     z    #    x     y     z\n");
			printf("---------------------#---------------------#---------------------\n");
		}
		
		/* Get the data from the device */
		if (!MPUGetAccel(&accelData)) {
			printf("Accelerometer error!\n");
			continue;
		} else if (!MPUGetGyro(&gyroData)) {
			printf("Gyroscope error!\n");
			continue;
		} else if (!MPUGetCompass(&cmpData)) {
			printf("Compass error!\n");
			continue;
		}
			
		gyroData = v3_sub(gyroData, gyroBias);
		
		cmpData = v3_sub(cmpData, cmpBias);
		
		angle = v3_add(angle, gyroData);
		
		heading = (int32_t)((float)atan((float)cmpData.y / (float)cmpData.x) * (float)(180.0 / M_PI));
		
		
		/* Print the data */
		//printf("%6ld,%6ld,%6ld, %6ld,%6ld,%6ld, %6ld,%6ld,%6ld\n",
		printf("%6ld %6ld %6ld #%6ld %6ld %6ld #%6ld %6ld %6ld\n",
			accelData.x, accelData.y, accelData.z,
			//gyroData.x, gyroData.y, gyroData.z,
			(int32_t)((float)angle.x*0.2*250./32678.), (int32_t)((float)angle.y*0.2*250./32678.), (int32_t)((float)angle.z*0.2*250./32678.0),
			cmpData.x, cmpData.y, heading);
		
		/* Delay */
        _delay_ms(P_SAMPLE_MS);
    }
	
	return 0;
}

uint8_t GetGyroBias(uint16_t nReadings, vector3 *bias)
{
	/* GetGyroBias(...)
	 * The function takes as input a number of readings and a reference to a vector3 
	 * structure. The function takes the specified number of gyroscope readings and 
	 * averages them, applying the result to the supplied vector. The function returns 
	 * its status: OKAY or ERROR.
	 * 
	 * Note: It is critical that the device is stationary during calibration.
	 *
	 * TGT
	 */
	vector3 sum = {0};
	vector3 gyroData;
	
	uint16_t readingCount = 0;
	
	for(readingCount = 0; readingCount < nReadings; readingCount++)	{
		// Get the data from the device
		if (!MPUGetGyro(&gyroData))
			return ERROR;	
		// Apply reading to sum for each axis
		sum = v3_add(sum, gyroData);
	}
	// Average each axis by number of readings
	*bias = v3_scale(sum, nReadings, VECT_DIV);
	return OKAY;
}

uint8_t GetMagBias(uint16_t nReadings, vector3* bias) {
	/* GetMagBias(...)
	 * The function takes as input a number of readings and a reference to a vector3 
	 * structure. The function takes the specified number of magnetometer readings and 
	 * calculates the magnetometer bias. The function returns its status: OKAY or ERROR.
	 * 
	 * Note: It is critical that the device is rotated thoroughly along each axis to get
	 * an accurate bias measurement.
	 */
	vector3 max = {0};
	vector3 min = {0};
	vector3 magData;
	
	uint16_t readingCount = 0;
	
	for(readingCount = 0; readingCount < nReadings; readingCount++)	{
		// Get the data from the device
		if (!MPUGetCompass(&magData))
			return ERROR;	
		// Check max and min for x-axis
		if (magData.x > max.x)
			max.x = magData.x;
		else if (magData.x < min.x)
			min.x = magData.x;
		// Check max and min for y-axis
		if (magData.y > max.y)
			max.y = magData.y;
		else if (magData.y < min.y)
			min.y = magData.y;
		// Check max and min for z-axis
		if (magData.z > max.z)
			max.z = magData.z;
		else if (magData.z < min.z)
			min.z = magData.z;
		_delay_ms(P_SAMPLE_MS);
	}
	// Average each axis by number of readings
	*bias = v3_scale(v3_add(max, min), 2, VECT_DIV);
	return OKAY;
}