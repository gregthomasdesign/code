// light-angle.c
// A standalone test program to experiment with angle calculations
// Author: Greg
// Date:   July 4, 2015

#include <math.h>
#include <stdio.h>
#include <stdint.h>

// Calculate the angle of the detected light
int8_t calculateAngle(uint8_t left, uint8_t right)
{
	// Find the total amount of light detected in both sensors
	int16_t total = right + left;
	
	// If the total is 0, then we don't know how to proceed -- return 0
	if(total == 0)
		return 0;
	
	// Find the difference between the right and left sensors
	// If this is positive, the light is mainly coming from the right
	// If negative, mainly from the left
	int16_t diff = right - left;
	
	// Compare the difference to the total
	// This will be 1 if the light is hard right and -1 if hard left
	double angle = (double) diff / (double) total;
	printf("%d %d %d %d %f\n", left, right, total, diff, angle);
	
	// Scale up to [-128, 128]
	return (int8_t)(angle * 128);
}

// Mainline: test this angle calculation
int main()
{
	printf("%d\n %d\n %d\n %d\n %d\n",
		calculateAngle(1, 1),
		calculateAngle(10, 5),
		calculateAngle(1, 255),
		calculateAngle(10, 128),
		calculateAngle(255, 1));
	return 0;
}