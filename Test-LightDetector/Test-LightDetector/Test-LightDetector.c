// Test-LightDetector.c
// The testing harness for the light detector
//
// Authors:	     Greg d'Eon / Thomas Gwynne-Timothy
// Release date: July 4, 2015

#define F_CPU	14745600UL

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <stdio.h>
#include <stdlib.h>
#include <util/delay.h>

#include "LightDetector.h"

/* SERIAL OUTPUT DECLARATIONS & DEFINITIONS */
static int uart_putchar(char c, FILE *stream);
static int uart_getchar(FILE *stream);

FILE mystdout = FDEV_SETUP_STREAM(uart_putchar, NULL, _FDEV_SETUP_WRITE);
FILE mystdin = FDEV_SETUP_STREAM(NULL, uart_getchar, _FDEV_SETUP_READ);

static int uart_putchar(char c, FILE *stream)
{
	loop_until_bit_is_set(UCSR0A, UDRE0);
	UDR0 = c;
	return 0;
}

static int uart_getchar(FILE *stream)
{
	loop_until_bit_is_set(UCSR0A, RXC0); /* Wait until data exists. */
	return UDR0;
}

void init_uart(void)
{
	UCSR0B = (1<<RXEN0) | (1<<TXEN0);
	UBRR0 = 7;
	stdout = &mystdout;
	stdin = &mystdin;
}


int main(void)
{
	// Initialize serial connection
	init_uart();	
	
	// Initialize light detector
	light_setup();
	sei();
	
    while(1)
    {
		// Start measuring
	    light_startMeasuring();
	    
	    // Wait until it's done
	    while(light_busy());
		
		// Report back
		printf("%d %d  %d %d  %d %d\n",
		light_magnitude(light_freq_500),
		light_direction(light_freq_500),
		light_magnitude(light_freq_1000),
		light_direction(light_freq_1000),
		light_magnitude(light_freq_2000),
		light_direction(light_freq_2000));
		
		// Wait a while
		_delay_ms(500);
    }
}