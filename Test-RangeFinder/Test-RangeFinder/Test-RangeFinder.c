/*
 * Test_RangeFinder.c
 *
 * Created: 2015-05-19 11:42:04 PM
 *  Author: Greg
 */ 

#include <avr/io.h>

#define F_CPU 14745600UL
#include <util/delay.h>

#include "RangeFinder.h"

//---- Things for testing the range finder -------------------------------------
#define LOOP_DELAY_MS	500

//---- Things for printing to PUTTY --------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <avr/pgmspace.h>

static int uart_putchar(char c, FILE *stream);
static int uart_getchar(FILE *stream);

FILE mystdout = FDEV_SETUP_STREAM(uart_putchar, NULL, _FDEV_SETUP_WRITE);
FILE mystdin = FDEV_SETUP_STREAM(NULL, uart_getchar, _FDEV_SETUP_READ);

static int uart_putchar(char c, FILE *stream)
{
	loop_until_bit_is_set(UCSR0A, UDRE0);
	UDR0 = c;
	return 0;
}

static int uart_getchar(FILE *stream)
{
	loop_until_bit_is_set(UCSR0A, RXC0); /* Wait until data exists. */
	return UDR0;
}

void init_uart(void)
{
	UCSR0B = (1<<RXEN0) | (1<<TXEN0);
	UBRR0 = 7;
	stdout = &mystdout;
	stdin = &mystdin;
}

int main(void)
{
	// Initialize UART connection
	init_uart();
	
	// Set up the range finder
	printf("Setting up rangefinder...\n");
	rangeFinder_setup();
	
	// Loop while range finder readings are taken
	printf("Starting measurement loop...\n");
    while(1)
    {
		// Read the range finder	
        uint16_t rangeFinderTime = rangeFinder_readBlocking();
		
		printf("time = %6d | distance = %6d\n", rangeFinderTime, rangeFinder_convert_us_cm(rangeFinderTime));
		
		// Delay before measuring distance again
		_delay_ms(LOOP_DELAY_MS);
    }
}
