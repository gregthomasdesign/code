/*
 * Test_PathFollowing.c
 *
 * Created: 7/04/15 10:47:41 AM
 *  Author: Thomas
 */ 

#define F_CPU 14745600UL

#include <avr/io.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <util/delay.h>

#include "Motor.h"
#include "MPU-9150.h"
#include "Vector.h"

// ---- SERIAL OUTPUT DECLARATIONS & DEFINITIONS ----------------------------------------
static int uart_putchar(char c, FILE *stream);
static int uart_getchar(FILE *stream);

FILE mystdout = FDEV_SETUP_STREAM(uart_putchar, NULL, _FDEV_SETUP_WRITE);
FILE mystdin = FDEV_SETUP_STREAM(NULL, uart_getchar, _FDEV_SETUP_READ);

static int uart_putchar(char c, FILE *stream)
{
	loop_until_bit_is_set(UCSR0A, UDRE0);
	UDR0 = c;
	return 0;
}

static int uart_getchar(FILE *stream)
{
	loop_until_bit_is_set(UCSR0A, RXC0); /* Wait until data exists. */
	return UDR0;
}

void init_uart(void)
{
	UCSR0B = (1<<RXEN0) | (1<<TXEN0);
	UBRR0 = 7;
	stdout = &mystdout;
	stdin = &mystdin;
}

#define SAMPLE_PERIOD	200

int main(void)
{
	int32_t dirCtrl = 0;
	int32_t dirActual = 0;
	int32_t dirError = 0;
	vector3 gyroReading = {0};
	vector3 gyroBias = {0};
	double dutyLeft = 0.4;
	double dutyRight = 0.4;
	
	// Enable interrupts
	sei();
	
	// Setup UART
	init_uart();
	
	// Initialize MPU 9150 sensor
	printf("Initializing sensor test...\n");
	if (!MPUInit(F_CPU, MPU9150_ACCEL_RANGE_2G, MPU9150_GYRO_RANGE_250DPS)) {
		printf("Error initializing MPU-9150.\n");
		return 1;
	}
		
	printf("Getting gyro bias...\n");
	if (!MPUGetGyroBias(200, &gyroBias))
		printf("Gyro calibration failed!\n");
	else
		printf("Bias: x = %ld, y = %ld, z = %ld\n", gyroBias.x, gyroBias.y, gyroBias.z);
	
	// Setup motors
	motor_init();
	
	motor_setDutyCycle(LEFT, dutyLeft);
	motor_setDutyCycle(RIGHT, dutyRight);
	motor_setDirection(LEFT,  CW);
	motor_setDirection(RIGHT, CW);
	
	motor_setFrequency(PSCL_64);
	
	// Enable motors
	motor_enable();
	
	// Loop while interrupts run
	while(1) 
	{
		/*
		_delay_ms(5000);
		motor_setDirection(LEFT, OFF);
		motor_setDirection(RIGHT, OFF);
		_delay_ms(5000);
		motor_setDirection(LEFT, CCW);
		motor_setDirection(RIGHT, CCW);
		_delay_ms(5000);
		motor_setDirection(LEFT, OFF);
		motor_setDirection(RIGHT, OFF);
		_delay_ms(5000);
		motor_setDirection(LEFT, CW);
		motor_setDirection(RIGHT, CW);
		*/
		
		// Wait
		_delay_ms(SAMPLE_PERIOD/2);
		// Get gyroscope reading
		MPUGetGyro(&gyroReading);
		// Correct data
		gyroReading = v3_sub(gyroReading, gyroBias);
		// Scale reading
		gyroReading = v3_scale(gyroReading, 2500, VECT_MULT);
		gyroReading = v3_scale(gyroReading, 32767, VECT_DIV);
		// Apply to direction 
		dirActual += gyroReading.z*SAMPLE_PERIOD/1000;
		// Get error
		dirError = dirCtrl - dirActual;
		// Update duty cycles
		dutyLeft -= (double)dirError/1000.;
		dutyRight += (double)dirError/1000.;
		printf("gyro reading: %ld\n", gyroReading.z);
		printf("actual direction: %ld\n",dirActual);
		//printf("duty left: %d/1000 | duty right: %d/1000\n", (int)dutyLeft*1000, (int)dutyRight*1000);
		//motor_setDutyCycle(LEFT, dutyLeft);
		//motor_setDutyCycle(RIGHT, dutyRight);
		_delay_ms(SAMPLE_PERIOD/2);
	}
}