#ifndef ENCODER_GREG_H
#define ENCODER_GREG_H

#include <stdint.h>

void encoder_setup();
void encoder_reset();

int32_t encoder_getLeft();
int32_t encoder_getRight();
void encoder_get(int32_t* leftPtr, int32_t* rightPtr);


#endif	// ENCODER_LIB_H