/*
 * blink.c
 *
 * Created: 5/19/15 3:36:04 PM
 *  Author: Thomas/Greg
 */ 

#include <avr/io.h>				// For controlling ports

#define F_CPU 14745600UL
#include <util/delay.h>			// For blocking delays

// Timing 
#define POLL_PERIOD_US 		1	// Time between polling checks
#define TRIGGER_PERIOD_US 	20	// Total time of the TRIG pulse

// Pins and registers used for range finder - change as necessary
#define TRIGGER_PIN		PC1
#define ECHO_PIN		PC0

#define RANGE_PORT		PORTC
#define RANGE_PIN		PINC
#define RANGE_DDR		DDRC

// ----- Private methods -------------------------------------------------------
void rangeFinder_trigger()
{
	// Set the TRIG pin high for long enough to start a read
	RANGE_PORT |= (1 << TRIGGER_PIN);
	_delay_us(TRIGGER_PERIOD_US);
	RANGE_PORT &= ~(1 << TRIGGER_PIN);
}


// ----- Public methods --------------------------------------------------------
// Sets up the pins for the range finder
void rangeFinder_setup()
{
	// Set trigger pin to output 
	RANGE_DDR |= (1 << TRIGGER_PIN);
	
	// Set echo pin to input
	RANGE_DDR &= ~(1 << ECHO_PIN);
}


// Gets the raw time (in us) of the range finder's ultrasonic echo
// Blocking method -- will never take more than a few ms
uint16_t rangeFinder_readBlocking()
{
	uint16_t ret = 0;
	
	// Trigger the range finder
	rangeFinder_trigger();
	
	// Poll the ECHO line until it goes high
	while(!(RANGE_PIN & (1 << ECHO_PIN)))
	{
		_delay_us(POLL_PERIOD_US);
	}
	
	// Poll the ECHO line until it goes low, tracking total time
	while(RANGE_PIN & (1 << ECHO_PIN))
	{
		ret += POLL_PERIOD_US;
		_delay_us(POLL_PERIOD_US);
	}
	// ECHO has gone low, so we're done
	return ret;
}


// Converts a raw time (in us) into a distance (in cm)
uint16_t rangeFinder_convert_us_cm(uint16_t time_us)
{
	return time_us / 58;
}
