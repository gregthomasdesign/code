// control.cpp
// The implementation of the robot control module.
//
// Author: Greg

#define F_CPU	14745600UL

#include "Control.h"
#include "Motor.h"
#include "Encoder.h"
#include "Status.h"

#include <stdio.h>
#include <util/delay.h>

// ----- Private function declarations -----------------------------------------
// Execute a single segment.
void control_runSegment(control_segment segment);

// Control loops for each type of segment
void control_stepStraight();
void control_stepLeft();
void control_stepRight();
void control_stepLeftBack();
void control_stepRightBack();
void control_delay();
void control_reset();

// Stop routine between segments
void control_stopWait();


// ----- Private data ----------------------------------------------------------
// Array of function pointers to each control loop
// Note: order is the same as the order in the control_setting enum
void (*stepArray[7])() = 
{
	control_stepStraight,
	control_stepLeft,
	control_stepRight,
	control_stepLeftBack,
	control_stepRightBack,
	control_delay,
	control_reset
};

// The list of path segments
control_segment* myPath;

// The amount of delay within the control loops, in ms
#define LOOP_DELAY		100

// The amount of delay between segments, in ms
#define SEGMENT_DELAY	500

// The size of one step in the feedback loop
#define GAIN_STRAIGHT	1

// The last known duty cycles for moving straight
uint8_t dutyStraightRef = 120;
uint8_t dutyStraightStep = 5;

// Duty cycle for left/right turns
uint8_t dutyTurn = 180;
uint8_t dutyTurnBack = 170;

// Angle error for turn correction
int16_t angleError = 0;

int32_t vRef = 3;
int32_t aRef = 0;

// Control variables
int32_t Kp_v_num = 0;
int32_t Kp_v_den = 1;

int32_t Ki_v_num = 0;
int32_t Ki_v_den = 1;

int32_t Kp_a_num = 5;
int32_t Kp_a_den = 1;

int32_t Ki_a_num = 1;
int32_t Ki_a_den = 2;

#define Kp_v	Kp_v_num/Kp_v_den
#define Ki_v	Ki_v_num/Ki_v_den
#define Kp_a	Kp_a_num/Kp_a_den
#define Ki_a	Ki_a_num/Ki_a_den


// ----- Public function implementation ----------------------------------------
// Set up the list of steps in the robot's movement
void control_setupPath(control_segment* path)
{
	// Save the pointer as our new path
	myPath = path;
}

// Get the controller to run
void control_run()
{
	// Run through array until we find an END
	for(int i = 0; myPath[i].setting != END; i++)
	{
		// Reset encoder ticks
		encoder_reset();
		
		// Decide which control loop to use
		void (*stepFunction)() = stepArray[myPath[i].setting];
		
		// Run it
		stepFunction(myPath[i].distance, myPath[i].timeout);
	}
	
	// Nothing left to do -- return
}


// ----- Private function implementation ---------------------------------------
// Move in a straight line
void control_stepStraight(uint16_t distance, uint32_t timeout)
{
	int32_t left;
	int32_t right;
	int32_t total, totalNew;
	int8_t rampComplete = 0;
	
	// Track segment time
	uint32_t timer = 0;
	
	// Get initial encoder values
	total = 0;

	// Enable motors with last known duty cycles
	motor_setDirection(LEFT,  CW);
	motor_setDirection(RIGHT, CW);
	motor_setDutyCycle(LEFT,  0);
	motor_setDutyCycle(RIGHT, 0);
	motor_enable();
	
	// Control parameters: velocity and angle (+ angle integer)
	int32_t v = 0;
	int32_t a = 0;
	int32_t aInt = 0;
		
	// Initialize control errors
	int32_t vErr = 0;
	int32_t aErr = 0;
	
	int32_t vErrInt = 0;
	int32_t aErrInt = 0;
		
	// Temporary duty cycles for control
	int32_t dutyTemp_left;
	int32_t dutyTemp_right;
	
	// Cut reference duty in half to start ramp
	dutyStraightRef = 3*dutyStraightRef/4;
	

	printf("%6d %6d\n", encoder_getLeft(), encoder_getRight());
	while(total < distance && timer <= timeout)
	{
		// Sample encoders
		left = encoder_getLeft();
		right = encoder_getRight();
		
		// Get new sum
		totalNew = left + right;
		
		// Calculate control parameters
		v = totalNew - total;
		a = right - left + angleError;
		aInt += a;
	
		total = totalNew;
		
		// Calculate errors
		vErr = vRef - v;
		vErrInt += vErr;
		aErr = aRef - a;
		aErrInt += v*aErr;
		
		// If in startup region, get next ramp value
		if ((v < vRef) && !rampComplete) {
			printf("%ld < %ld and %d == 0\n", v, vRef, rampComplete);
			dutyStraightRef += dutyStraightStep;
		} else 
			rampComplete = 1;

		dutyTemp_left = dutyStraightRef - aErr*Kp_a - aErrInt*Ki_a;
		dutyTemp_right = dutyStraightRef + aErr*Kp_a + aErrInt*Ki_a;
		
	
		// Clamp duty cycles
		if(dutyTemp_left < 0) dutyTemp_left = 0;
		if(dutyTemp_right < 0) dutyTemp_right = 0;
		if(dutyTemp_left  > 220) dutyTemp_left  = 220;
		if(dutyTemp_right > 220) dutyTemp_right = 220;
		
		// Apply new duty cycles
		motor_setDutyCycle(LEFT,  dutyTemp_left);
		motor_setDutyCycle(RIGHT, dutyTemp_right);
		
		// Wait
		//printf("%6ld %6ld %6ld %6ld %6ld %6ld\n", total, a, -aErrInt, v, dutyTemp_left, dutyTemp_right); //dutyStraight_left, dutyStraight_right);
		printf("timer = %ld, timeout = %ld\n", timer, timeout);
		
		// Wait for sampling delay
		_delay_ms(LOOP_DELAY);
		
		if (timeout != 0) timer += LOOP_DELAY;
	}
	// Stop and wait
	control_stopWait();
	
	// Calculate error
	angleError = encoder_getRight() - encoder_getLeft() + angleError;
}

// Turn left
void control_stepLeft(uint16_t distance, uint32_t timeout)
{
	// Track segment time
	uint32_t timer = 0;
	
	// Get target distance
	int16_t final = distance - angleError;
	// Clamp it to non-negative
	if (final < 0) final = 0;
	
	// Turn on right motor at dutyTurn
	motor_setDutyCycle(RIGHT, dutyTurn);
	motor_setDirection(RIGHT, CW);
	motor_setDirection(LEFT,  BRAKE);
	
	motor_enable();
	
	// Poll the distance until we get there
	while(encoder_getRight() < final && timer <= timeout) {
		// Wait and increment timer
		_delay_ms(LOOP_DELAY);
		if (timeout != 0) timer += LOOP_DELAY;
	}
	
	// Stop and wait
	control_stopWait();
	
	// Calculate error
	angleError = encoder_getRight() - encoder_getLeft() - final;
}

// Turn right
void control_stepRight(uint16_t distance, uint32_t timeout)
{
	// Track segment time
	uint32_t timer = 0;
		
	// Get target value
	int16_t final = distance + angleError;
	// Clamp it to non-negative
	if (final < 0) final = 0;
	
	// Turn on left motor
	motor_setDutyCycle(LEFT,  dutyTurn);
	motor_setDirection(LEFT,  CW);
	motor_setDirection(RIGHT, BRAKE);
	
	motor_enable();
	
	// Poll the encoder until we're done
	while(encoder_getLeft() < final && timer <= timeout) {
		// Wait and increment timer
		_delay_ms(LOOP_DELAY);
		if (timeout != 0) timer += LOOP_DELAY;
	}
		
	// Stop and wait
	control_stopWait();
	
	// Calculate error
	angleError = final - (encoder_getLeft() - encoder_getRight());
}

void control_stepLeftBack(uint16_t distance, uint32_t timeout)
{
	// Track segment time
	uint32_t timer = 0;
	
	// Get target distance
	int16_t final = distance - angleError;
	// Clamp it to non-negative
	if (final < 0) final = 0;
	
	// Turn on right motor at dutyTurn
	motor_setDutyCycle(LEFT,  dutyTurnBack);
	motor_setDirection(LEFT,  CCW);
	motor_setDirection(RIGHT, BRAKE);
	
	motor_enable();
	
	// Poll the distance until we get there
	while(encoder_getLeft() < final && timer <= timeout) {
		// Wait and increment timer
		_delay_ms(LOOP_DELAY);
		if (timeout != 0) timer += LOOP_DELAY;
	}
	
	// Stop and wait
	control_stopWait();
	
	// Calculate error
	angleError = (encoder_getLeft() - encoder_getRight()) - final ;
}





void control_reset(uint16_t distance, uint32_t timeout) {
	// Reset angle error
	angleError = 0;
	// Reset encoders
	encoder_reset();
}



void control_stepRightBack(uint16_t distance, uint32_t timeout)
{
	// Track segment time
	uint32_t timer = 0;
	
	// Get target distance
	int16_t final = distance + angleError;
	// Clamp it to non-negative
	if (final < 0) final = 0;
	
	// Turn on right motor at dutyTurn
	motor_setDutyCycle(RIGHT, dutyTurnBack);
	motor_setDirection(RIGHT, CCW);
	motor_setDirection(LEFT,  OFF);
	
	motor_enable();
	
	// Poll the distance until we get there
	while(encoder_getRight() < final && timer <= timeout) {
		// Wait and increment timer
		_delay_ms(LOOP_DELAY);
		if (timeout != 0) timer += LOOP_DELAY;
	}
	
	// Stop and wait
	control_stopWait();
	
	// Calculate error
	angleError = final - (encoder_getRight() - encoder_getLeft());
}

void control_delay(uint16_t distance, uint32_t timeout) {
	uint32_t timer = 0;
	LED_set(LED_BLUE);
	LED_clear(LED_RED);
	
	while(timer < timeout) {
		// Toggle LEDs to indicate status
		LED_toggle(LED_BLUE);
		LED_toggle(LED_RED);
		// Wait and increment timer
		_delay_ms(LOOP_DELAY);
		timer += LOOP_DELAY;
	}
	
	// Shutoff LEDs to indicate delay has finished
	LED_clear(LED_BLUE);
	LED_clear(LED_RED);
}

void control_stopWait() {	
	// Turn off motors
	motor_setDirection(LEFT, BRAKE);
	motor_setDirection(RIGHT, BRAKE);
	motor_setDutyCycle(LEFT, 0xFF);
	motor_setDutyCycle(RIGHT, 0xFF);
		
	// Wait between segments
	_delay_ms(SEGMENT_DELAY);
}