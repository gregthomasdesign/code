// LightDetector.c
// The implementation of the light detector module
//
// Authors:	     Greg d'Eon / Thomas Gwynne-Timothy
// Release date: July 4, 2015

#include "LightDetector.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <stdio.h>
#include <math.h>

// ----- Light detector parameters
// Prescaled clock frequency
#define LIGHT_CLOCK_FREQ	(14745600. / 128.)
// Frequency of sampling (one sample every 13 ticks)
#define LIGHT_SAMPLE_FREQ	(LIGHT_CLOCK_FREQ / 13.)

// Left vs. right definitions
#define LEFT	0
#define RIGHT	1

// ----- Global variables for the module
uint16_t LIGHT_NUM_POINTS =	400;			// Points per measurement (less for speed, more for rejection)
uint16_t nextPoint;							// The number of points we've measured so far
volatile uint8_t isBusy;					// Boolean: whether we're working or not
volatile uint8_t measuringLR;				// Boolean: measuring left or right side
double magnitudeArray[light_freq_number][2];// List of calculated magnitudes

double d[light_freq_number][3];				// The array used for the Goertzel algorithm
double frequencyArray[light_freq_number] =	// List of frequencies, as doubles
	{ 500.0, 1000.0, 2000. };
double coeffArray[light_freq_number];		// List of Goertzel coefficients


// ----- Private function prototypes
void light_stopMeasuring(void);

// ----- Implementation
// ADC ISR: process data and decide whether or not to continue
ISR(ADC_vect)
{
	// Read data: low byte, then high byte
	uint16_t data = ADCL;
	data |= (ADCH << 8);
	
	// Process data - fast enough to do in ISR
	for(int i = 0; i < light_freq_number; i++)
	{
		d[i][0] = data + coeffArray[i] * d[i][1] - d[i][2];
		d[i][2] = d[i][1];
		d[i][1] = d[i][0];
	}
		
	// Check if finished and stop if we are
	nextPoint++;
	if(nextPoint >= LIGHT_NUM_POINTS)
		light_stopMeasuring();
}

// Turn on the ADC and let it run in free-running mode
void light_startMeasuring(void)
{
	// Now we're busy
	isBusy = 1;
	// Reset the number of measurements we've taken
	nextPoint = 1;
	
	// Reset the Goertzel array
	for(int i = 0; i < light_freq_number; i++)
	{
		for(int j = 0; j < 3; j++)
		{
			d[i][j] = 0;
		}
	}
	
	// Turn on the ADC and tell it to start
	ADCSRA |= (1 << ADEN) | (1 << ADSC);
	
	// Let the interrupts take over
}

// Stop the ADC -- called from the ISR
void light_stopMeasuring(void)
{
	// Turn off the ADC
	ADCSRA &= ~(1 << ADEN);
	
	// Calculate the magnitudes
	for(int i = 0; i < light_freq_number; i++)
	{
		double d1 = d[i][1];
		double d2 = d[i][2];
		double coeff = coeffArray[i];
		
		double mag = d1*d2*coeff;
		mag = d1*d1 + d2*d2 - mag;
		magnitudeArray[i][measuringLR] = sqrt(mag) / (LIGHT_NUM_POINTS / 2.);
	}
	
	if(measuringLR == LEFT)
	{
		// If we just finished measuring the left side, measure the right
		ADMUX |= (1 << MUX0);		// Switch to input PA0
		light_startMeasuring();		// Start the measurement
		measuringLR = RIGHT;		// Make a note that we're switching
	}
	else // RIGHT
	{
		// Switch back to the left side for next time
		ADMUX &= ~(1 << MUX0);
		measuringLR = LEFT;
		// Tell the world we're done
		isBusy = 0;	
	}
}

// Set up the light detector's environment and data
void light_setup(void)
{
	// ADC register setup
	ADMUX = (0 << REFS1) | (1 << REFS0)		// Use AVCC as V_REF
	      | (0 << ADLAR)					// No right shift
		  | (0 << MUX4) | (0 << MUX3)		// Use input PA0
		  | (0 << MUX2) | (0 << MUX1)		//
		  | (0 << MUX0);					// 
		  
	ADCSRA = (0 << ADEN)					// Don't enable the ADC yet
	       | (0 << ADSC)					// Don't start converting yet
		   | (1 << ADATE)					// Trigger on event
		   | (1 << ADIE)					// Enable interrupts
		   | (1 << ADPS2) | (1 << ADPS1)	// /128 prescale
		   | (1 << ADPS0);					//
	
	ADCSRB = (0 << ADTS2) | (0 << ADTS1)	// Free-running mode
	       | (0 << ADTS0);
		   
	DIDR0 = 0;								// Don't disable input buffers

	// Goertzel coefficient setup
	for(int i = 0; i < light_freq_number; i++)
	{
		uint16_t k = LIGHT_NUM_POINTS * frequencyArray[i] / LIGHT_SAMPLE_FREQ + 0.5;
		coeffArray[i] = 2. * cos(2. * 3.1415926536 * k / LIGHT_NUM_POINTS);
	}
}

// Change the number of points per measurement
void light_setNumPoints(uint16_t points)
{
	LIGHT_NUM_POINTS = points;
}

// Returns whether the light detector code is running
uint8_t light_busy(void)
{
	return isBusy;
}

// Return the magnitude of the detected light
uint16_t light_magnitude(light_freq freq)
{
	// Take the average of the two channels
	return (magnitudeArray[freq][RIGHT] + magnitudeArray[freq][LEFT]) / 2;
}

// Return the direction of the detected light
// Calculation: (right - left) / (right + left) * 128
int8_t light_direction(light_freq freq)
{
	// Find the total amount of light detected in both sensors
	int16_t total = magnitudeArray[freq][RIGHT] + magnitudeArray[freq][LEFT];
	
	// If the total is 0, then we don't know how to proceed -- return 0
	if(total == 0)
		return 0;
	
	// Find the difference between the right and left sensors
	// This is positive if light is on the right and negative if left
	int16_t diff = magnitudeArray[freq][RIGHT] - magnitudeArray[freq][LEFT];
	
	// Compare the difference to the total
	// This will be 1 if the light is hard right and -1 if hard left
	double angle = (double) diff / (double) total;
	
	// Scale up to [-128, 128]
	int8_t ret = angle * 128;
	
	// Check for overflow
	if(ret < 0 && angle > 0)
		return 0x7F;
	return (int8_t)(angle * 128);	
}
