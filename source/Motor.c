// Motor.c
// Implementation of the motor PWM module

// Possible improvements:
// - Could use automatic pin sets to avoid ISRs,
//   but this forces us to use PB3/4 -- might be an issue
// - May not need to change the PWM frequency -- hide from user?
// 

#include "Motor.h"

#include <avr/io.h>
#include <avr/interrupt.h>

// Pins used for motor drivers
#define motorPort		PORTC
#define motorDDR		DDRC
#define motorOutLeftA	(1 << 2)
#define motorOutLeftB	(1 << 3)
#define motorOutRightA	(1 << 4)
#define motorOutRightB	(1 << 5)

// 8 bit counter -- duty cycles are a fraction of 256
#define motorMaxDutyCycle	(1 << 8)

// Lookup table and mask for timer prescale bits
#define timerPrescaleMask	(0x07)
uint8_t timerPrescaleTable[] = {
	0x01,
	0x02,
	0x03,
	0x04,
	0x05,
	0x00
};

// Lookup table for active pin masks
uint8_t outputMaskTable[] = {
	0x3C,	// 0b0011.1100
	0x14,	// 0b0001.0100
	0x00,	// 0b0000.0000
	0x28,	// 0b0010.1000
};

// ----- Variables ------------------------------------------
// Frequency used when the motor is enabled
motor_freq selectedFreq;

// Masks to stop from enabling inactive pins
uint8_t motorOutLeftMask, motorOutRightMask;

// ----- ISRs -----------------------------------------------
ISR(TIMER0_COMPA_vect)
{
	// Set all left outputs high
	motorPort |= (motorOutLeftA | motorOutLeftB);
}

ISR(TIMER0_COMPB_vect)
{
	// Set all right outputs high
	motorPort |= (motorOutRightA | motorOutRightB);
}

ISR(TIMER0_OVF_vect)
{
	// Set active outputs low
	// (Check which outputs are active using the left/right masks)
	motorPort &= ~((motorOutLeftMask  & (motorOutLeftA  | motorOutLeftB)) 
	             | (motorOutRightMask & (motorOutRightA | motorOutRightB)));
	//	motorPort &= ~(motorOutLeftA | | motorOutRightA;
}


// ----- Public functions -----------------------------------
void motor_init()
{
	// Set the motor driver pins as outputs
	motorDDR |= motorOutLeftA  | motorOutLeftB
	          | motorOutRightA | motorOutRightB;

	// Set up timer0 in normal mode with interrupts on compare A and B
	TCCR0A = 0;
	TCCR0B = 0;
	TIMSK0 = (1 << OCIE0B) | (1 << OCIE0A) | (1 << TOIE0);
	
	// Set a default frequency and direction
	selectedFreq = PSCL_1024;
	motor_setDirection(LEFT,  OFF);
	motor_setDirection(RIGHT, OFF);
}

void motor_setDutyCycle(motor_side side, uint8_t dutyCycle)
{
	// Load this duty cycle into the correct compare register
	switch(side){
	case LEFT:
		OCR0A = dutyCycle;
		break;
		
	case RIGHT:
		OCR0B = dutyCycle;
		break;
		
	default:
		// Unrecognized input
		break;
	}
}

void motor_setDirection(motor_side side, motor_direction direction)
{
	// Look up which bits should be active now
	uint8_t mask = outputMaskTable[direction];

	// Assign this to left or right
	switch(side){
	case LEFT:
		motorOutLeftMask = mask;
		break;
	
	case RIGHT:
		motorOutRightMask = mask;
		break;
	
	default:
		// Unrecognized input
		break;
	}
}


void motor_setFrequency(motor_freq freq)
{
	// Update the selected frequency
	selectedFreq = freq;
}

void motor_enable()
{
	// Clear clock prescale bits
	motor_disable();
	
	// Set the bits with the correct values
	TCCR0B |= timerPrescaleTable[selectedFreq];
}

void motor_disable()
{
	// Clear the prescale bits to turn the timer off	
	TCCR0B &= ~timerPrescaleMask;
	
	// Set both outputs high (motor off)
	motorPort |= (motorOutLeftA | motorOutLeftB)
				| (motorOutRightA | motorOutRightB);
}
