
#include "Vector.h"


vector3 v3_add(vector3 x1, vector3 x2)
{
	return (vector3){
		x1.x + x2.x,
		x1.y + x2.y,
		x1.z + x2.z
	};
}

vector3 v3_sub(vector3 x1, vector3 x2)
{
	return (vector3){
		x1.x - x2.x,
		x1.y - x2.y,
		x1.z - x2.z
	};
}

vector3 v3_scale(vector3 x1, int32_t k, uint8_t mode)
{
	switch(mode)
	{
	case VECT_DIV:
		return (vector3){
			x1.x/k,
			x1.y/k,
			x1.z/k
		};
	default:
		return (vector3){
			k*x1.x,
			k*x1.y,
			k*x1.z
		};
	}
}
