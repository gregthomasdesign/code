// LightDetector.h
// The interface for the light detector module
// Uses pins PA0 (left detector) and PA1 (right detector) along with the ADC 
//
// Authors:	     Greg d'Eon / Thomas Gwynne-Timothy
// Release date: July 4, 2015

#ifndef LIGHT_DETECTOR_H
#define LIGHT_DETECTOR_H

#include <stdint.h>

// The three frequencies that can be detected by the module
typedef enum light_freq{
	light_freq_500,
	light_freq_1000,
	light_freq_2000,
	light_freq_number,	// Number of frequencies available
} light_freq;

// Set up the light detector's environment
// Should be called once during program startup
void light_setup(void);

// Change the number of points per measurement
// More points gives better frequency rejection (narrower passband)
// Less points takes less time
void light_setNumPoints(uint16_t points);

// Tell the light detector to run one measurement
void light_startMeasuring(void);

// Return whether the light detector is busy
uint8_t light_busy(void);

// Return the magnitude of the most recently detected light 
uint16_t light_magnitude(light_freq freq);

// Return the direction of the most recently detected light
int8_t light_direction(light_freq freq);

#endif // LIGHT_DETECTOR_H