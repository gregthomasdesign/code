// control.h
// The main control code. 
// Uses encoder feedback to adjusts the motor duty cycles.
//
// Author: Greg 

#ifndef CONTROL_H
#define CONTROL_H

#include <inttypes.h>

// The different directions that the robot can move in
typedef enum control_setting
{
	STRAIGHT,
	TURN_LEFT,
	TURN_RIGHT,
	TURN_LEFT_BACK,
	TURN_RIGHT_BACK,
	DELAY,
	RESET,
	END,
} control_setting;


// A single segment in the robot's path
typedef struct control_segment
{
	control_setting setting;	// Which way to move
	uint16_t distance;			// How far to go
	uint32_t timeout;
} control_segment;


// Set up the list of steps in the robot's movement
void control_setupPath(control_segment* path);

// Get the controller to run. Returns after encountering an END.
void control_run();




#endif	// CONTROL_H
