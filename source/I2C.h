#ifndef I2C_H
#define I2C_H

#include <stdint.h>

/* TWI Interface */
// Initialize connection
void TWIInit(uint32_t, uint32_t);
// Generate start/stop signals
void TWIStart(void);
void TWIStop(void);
// Write byte of data
void TWIWrite(uint8_t);
// Read data (with/without acknowledge)
uint8_t TWIReadAck(void);
uint8_t TWIReadNack(void);
// Read device status
uint8_t TWIGetStatus(void);

#endif // I2C_H