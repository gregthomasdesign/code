
#include "Compass.h"


#include "MPU-9150.h"

/* COMPASS INTERFACE */
uint8_t ReadCompass(uint16_t *dataX, uint16_t *dataY, uint16_t *dataZ)
{
	uint8_t tempHi, tempLo;
	uint8_t errorCount = 0;
	
	errorCount += MPUReadRegister(MPU9150_ACCEL_XOUT_H, &tempHi);
	errorCount += MPUReadRegister(MPU9150_ACCEL_XOUT_L, &tempLo);
	*dataX = ((uint16_t)tempHi << 8) | tempLo;
	
	errorCount += MPUReadRegister(MPU9150_ACCEL_YOUT_H, &tempHi);
	errorCount += MPUReadRegister(MPU9150_ACCEL_YOUT_L, &tempLo);
	*dataY = ((uint16_t)tempHi << 8) | tempLo;
	
	errorCount += MPUReadRegister(MPU9150_ACCEL_ZOUT_H, &tempHi);
	errorCount += MPUReadRegister(MPU9150_ACCEL_ZOUT_L, &tempLo);
	*dataZ = ((uint16_t)tempHi << 8) | tempLo;
	return errorCount;
}

uint8_t InitCompass()
{
	uint8_t errorCount = 0;
	
	/* Get the device out of sleep mode */
	errorCount += MPUWriteRegister(MPU9150_PWR_MGMT_1, 0);
	/* Enable I2C pass through mode */
	errorCount += MPUWriteRegister(0x6A, 0);
	errorCount += MPUWriteRegister(0x37, 0x02);
	
	return errorCount;
}

