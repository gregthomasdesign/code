// Motor.h
// The motor driver PWM module. 
// Provides an interface for driving the motors on the robot at variable speeds.

#ifndef MOTOR_H
#define MOTOR_H

#include <inttypes.h>

// The two output options for setDutyCycle
typedef enum motor_side{
	LEFT,
	RIGHT,	
} motor_side;

// The directions that the motor can move in
typedef enum motor_direction{
	CW,
	CCW,
	OFF,
	BRAKE,
} motor_direction;

// The possible frequencies for the PWM output
typedef enum motor_freq{
	PSCL_1,		// 57600 Hz
	PSCL_8,		//  7200 Hz
	PSCL_64,	//   900 Hz
	PSCL_256,	//   225 Hz
	PSCL_1024,  //    56 Hz
	DISABLE,
} motor_freq;

// Sets up the motor driver module. Should be called once.
void motor_init();

// Sets the duty cycle for one motor driver
void motor_setDutyCycle(motor_side side, uint8_t dutyCycle);

// Sets the motor direction for one side
void motor_setDirection(motor_side side, motor_direction direction);

// Sets the PWM frequency for both motors
// Can only be chosen from a list of prescalers
void motor_setFrequency(motor_freq freq);


// Turns on the module's timer and start running the motors
void motor_enable();

// Turns off the motors
void motor_disable();

// TODO: add interface for forward/reverse/stopping


#endif // MOTOR_H