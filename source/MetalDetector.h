// MetalDetector.h
// The metal detector module.
// Drives a tank oscillator with a square wave and measures the 
//   output amplitude to detect the presence of metal. 

// Notes on tuning metal detector:
//   The potentiometer on the breadboard is used to generate a DC comparison value.
//   This should be adjusted so that the output with no metal barely stays low
//   (ie: with any metal nearby, the output of the comparator goes high)

#ifndef METAL_H
#define METAL_H

#include <stdint.h>


// Set up the metal detector peripherals.
// Should be called once.
void metal_init();

// Turn on the metal detector square wave output.
void metal_enable();

// Turn off the metal detector square wave output to save power.
void metal_disable();

// Read the metal detector's comparator output.
// Returns true if metal is present or false if it is not.
uint8_t metal_detect();


#endif // METAL_H