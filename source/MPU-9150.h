#ifndef MPU9150_H
#define MPU9150_H

#include <stdint.h>

#define		OKAY	1
#define		ERROR	0

#define		READ	1
#define		WRITE	0

#include "Vector.h"

/* DEVICE REGISTERS */
#define		MPU9150_ACC_GYRO_ADDR	0x68
#define		MPU9150_CMP_ADDR		0x0C
#define		MPU9150_CMP_STATUS		0x02

#define		MPU9150_ACCEL_CONFIG	0x1C
#define		MPU9150_GYRO_CONFIG		0x1B

#define		MPU9150_CMP_XOUT_H		0x04
#define		MPU9150_CMP_XOUT_L		0x03
#define		MPU9150_CMP_YOUT_H		0x06
#define		MPU9150_CMP_YOUT_L		0x05
#define		MPU9150_CMP_ZOUT_H		0x08
#define		MPU9150_CMP_ZOUT_L		0x07

#define		MPU9150_ACCEL_XOUT_H	0x3B
#define		MPU9150_ACCEL_XOUT_L	0x3C
#define		MPU9150_ACCEL_YOUT_H	0x3D
#define		MPU9150_ACCEL_YOUT_L	0x3E
#define		MPU9150_ACCEL_ZOUT_H	0x3F
#define		MPU9150_ACCEL_ZOUT_L	0x40

#define		MPU9150_GYRO_XOUT_H		0x43
#define		MPU9150_GYRO_XOUT_L		0x44
#define		MPU9150_GYRO_YOUT_H		0x45
#define		MPU9150_GYRO_YOUT_L		0x46
#define		MPU9150_GYRO_ZOUT_H		0x47
#define		MPU9150_GYRO_ZOUT_L		0x48

#define		MPU9150_PWR_MGMT_1		0x6B

/* ACCELEROMETER RANGE */
#define		MPU9150_ACCEL_RANGE_2G		0 << 3
#define		MPU9150_ACCEL_RANGE_4G		1 << 3
#define		MPU9150_ACCEL_RANGE_8G		2 << 3
#define		MPU9150_ACCEL_RANGE_16G		3 << 3

/* GYROSCOPE SENSITIVITY */
#define		MPU9150_GYRO_RANGE_250DPS	0 << 3
#define		MPU9150_GYRO_RANGE_500DPS	1 << 3
#define		MPU9150_GYRO_RANGE_1000DPS	2 << 3
#define		MPU9150_GYRO_RANGE_2000DPS	3 << 3

/* DEVICE DATA RATE */
#define		MPU9150_SCL_CLOCK		400000UL

/* DEVICE FUNCTIONS */
uint8_t MPUInit(uint32_t cpu_clock, uint8_t accel_setting, uint8_t gyro_setting);
uint8_t MPUGetCompass(vector3 *data);
uint8_t MPUGetGyro(vector3 *data);
uint8_t MPUGetAccel(vector3 *data);
uint8_t MPUGetGyroBias(uint16_t nReadings, vector3 *bias);

#endif // MPU9150_H