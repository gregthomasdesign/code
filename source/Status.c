// Status.c
// The implementation of the status LED methods

#include "Status.h"
#include <avr/io.h>
#include <stdlib.h>

#define	LED_PORT	PORTD
#define	LED_DDR		DDRD

// Array of pins to toggle for each LED
uint8_t LEDPins[] = 
{
	0x80,	// LED_BLUE: 0100.0000
	0x40,	// LED_RED:  1000.0000
};

// Setup: set the LED pins as outputs
void LED_init()
{
	uint8_t pins = (LEDPins[LED_BLUE] | LEDPins[LED_RED]);
	LED_PORT &= ~pins;
	LED_DDR |= pins;
}


// Public methods to set/clear these LEDs
void LED_set(LED_COLOR LED)
{
	LED_PORT |= LEDPins[LED];
}

void LED_clear(LED_COLOR LED)
{
	LED_PORT &= ~LEDPins[LED];
}

void LED_toggle(LED_COLOR LED)
{
	LED_PORT ^= LEDPins[LED];
}
