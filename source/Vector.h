#ifndef VECTOR_H
#define VECTOR_H
#include <stdint.h>

#define VECT_MULT	0
#define VECT_DIV	1


typedef struct vector2
{
	int32_t x;
	int32_t y;
} vector2;

typedef struct vector3
{
	int32_t x;
	int32_t y;
	int32_t z;
} vector3;

vector3 v3_add(vector3, vector3);
vector3 v3_sub(vector3, vector3);
vector3 v3_scale(vector3, int32_t, uint8_t);

#endif // VECTOR_H

