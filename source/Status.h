// Status.h
// Some basic methods for blinking LEDs to let the robot tell us its status

#ifndef	STATUS_H
#define	STATUS_H

// A list of the LEDs we can handle
typedef enum LED_COLOR
{
	LED_BLUE,
	LED_RED,
} LED_COLOR;

// Setup for the LEDs
void LED_init();

// Public methods to set/clear these LEDs
void LED_set   (LED_COLOR LED);
void LED_clear (LED_COLOR LED);
void LED_toggle(LED_COLOR LED);

#endif	// STATUS_H
