#ifndef COMPASS_H
#define COMPASS_H

#include <stdint.h>

/* COMPASS INTERFACE */
uint8_t ReadCompass(uint16_t *, uint16_t *, uint16_t *);
uint8_t InitCompass();

#endif // COMPASS_H
