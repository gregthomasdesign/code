/*
 * RangeFinder.h
 *
 * Created: 5/19/15 3:36:04 PM
 *  Author: Thomas/Greg
 */ 

// Include guard
#ifndef RANGEFINDER_H
#define RANGEFINDER_H

// Sets up the pins for the range finder
void rangeFinder_setup();

// Gets the raw time (in us) of the range finder's ultrasonic echo
// Blocking method -- will never take more than a few ms
uint16_t rangeFinder_readBlocking();

// Converts a raw time (in us) into a distance (in cm)
uint16_t rangeFinder_convert_us_cm(uint16_t time_us);

#endif // RANGEFINDER_H