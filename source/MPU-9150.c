
#include "MPU-9150.h"
#include "I2C.h"

/* INTERNAL FUNCTION DECLARATIONS */
static uint8_t MPUReadRegister(uint8_t, uint8_t, uint8_t *);
static uint8_t MPUWriteRegister(uint8_t, uint8_t, uint8_t);

/* EXTERNAL FUNCTION DEFINITIONS */
uint8_t MPUInit(uint32_t cpu_clock, uint8_t accel_setting, uint8_t gyro_setting)
{
	/* Initialize two-wire interface */
	TWIInit(MPU9150_SCL_CLOCK, cpu_clock);
	/* Get the device out of sleep mode */
	if (!MPUWriteRegister(MPU9150_ACC_GYRO_ADDR, MPU9150_PWR_MGMT_1, 0))
		return ERROR;
	/* Configure sensitivity */
	if (!MPUWriteRegister(MPU9150_ACC_GYRO_ADDR, MPU9150_ACCEL_CONFIG, accel_setting))
		return ERROR;
	if (!MPUWriteRegister(MPU9150_ACC_GYRO_ADDR, MPU9150_GYRO_CONFIG, gyro_setting))
		return ERROR;
	/* Enable I2C pass through mode */
	if (!MPUWriteRegister(MPU9150_ACC_GYRO_ADDR, 0x6A, 0))
		return ERROR;
	if (!MPUWriteRegister(MPU9150_ACC_GYRO_ADDR, 0x37, 0x02))
		return ERROR;
	
	return OKAY;
}

uint8_t MPUGetGyro(vector3 *vect)
{
	uint8_t tempHi, tempLo;
	
	/* Get the x-axis data */
	if (!MPUReadRegister(MPU9150_ACC_GYRO_ADDR, MPU9150_GYRO_XOUT_H, &tempHi))
		return ERROR;
	if (!MPUReadRegister(MPU9150_ACC_GYRO_ADDR, MPU9150_GYRO_XOUT_L, &tempLo))
		return ERROR;
	vect->x = (tempHi << 8) | tempLo;
	
	/* Get the y-axis data */
	if (!MPUReadRegister(MPU9150_ACC_GYRO_ADDR, MPU9150_GYRO_YOUT_H, &tempHi))
		return ERROR;
	if (!MPUReadRegister(MPU9150_ACC_GYRO_ADDR, MPU9150_GYRO_YOUT_L, &tempLo))
		return ERROR;
	vect->y = (tempHi << 8) | tempLo;
	
	/* Get the z-axis data */
	if (!MPUReadRegister(MPU9150_ACC_GYRO_ADDR, MPU9150_GYRO_ZOUT_H, &tempHi))
		return 0;
	if (!MPUReadRegister(MPU9150_ACC_GYRO_ADDR, MPU9150_GYRO_ZOUT_L, &tempLo))
		return 0;
	vect->z = (tempHi << 8) | tempLo;
	
	return OKAY;
}

uint8_t MPUGetCompass(vector3 *vect)
{
	uint8_t tempHi, tempLo;
	uint8_t status;
	
	/* Trigger readings */
	if (!MPUWriteRegister(MPU9150_CMP_ADDR, 0x0A, 0x01))
		return ERROR;
	
	status = 0;
	
	/* Loop until compass ready */
	while(!(status & 0x01)) {
		MPUReadRegister(MPU9150_CMP_ADDR, MPU9150_CMP_STATUS, &status);
	}
	
	/* Get the x-axis data */
	if (!MPUReadRegister(MPU9150_CMP_ADDR, MPU9150_CMP_XOUT_H, &tempHi))
		return ERROR;
	if (!MPUReadRegister(MPU9150_CMP_ADDR, MPU9150_CMP_XOUT_L, &tempLo))
		return ERROR;
	vect->x = (tempHi << 8) | tempLo;
	
	/* Get the y-axis data */
	if (!MPUReadRegister(MPU9150_CMP_ADDR, MPU9150_CMP_YOUT_H, &tempHi))
		return ERROR;
	if (!MPUReadRegister(MPU9150_CMP_ADDR, MPU9150_CMP_YOUT_L, &tempLo))
		return ERROR;
	vect->y = (tempHi << 8) | tempLo;
	
	/* Get the z-axis data */
	if (!MPUReadRegister(MPU9150_CMP_ADDR, MPU9150_CMP_ZOUT_H, &tempHi))
		return ERROR;
	if (!MPUReadRegister(MPU9150_CMP_ADDR, MPU9150_CMP_ZOUT_L, &tempLo))
		return ERROR;
	vect->z = (tempHi << 8) | tempLo;
	
	return OKAY;
}

uint8_t MPUGetAccel(vector3 *vect)
{
	uint8_t tempHi, tempLo;
	
	/* Get the x-axis data */
	if (!MPUReadRegister(MPU9150_ACC_GYRO_ADDR, MPU9150_ACCEL_XOUT_H, &tempHi))
		return ERROR;
	if (!MPUReadRegister(MPU9150_ACC_GYRO_ADDR, MPU9150_ACCEL_XOUT_L, &tempLo))
		return ERROR;
	vect->x = (tempHi << 8) | tempLo;
	
	/* Get the y-axis data */
	if (!MPUReadRegister(MPU9150_ACC_GYRO_ADDR, MPU9150_ACCEL_YOUT_H, &tempHi))
		return ERROR;
	if (!MPUReadRegister(MPU9150_ACC_GYRO_ADDR, MPU9150_ACCEL_YOUT_L, &tempLo))
		return ERROR;
	vect->y = (tempHi << 8) | tempLo;
	
	/* Get the z-axis data */
	if (!MPUReadRegister(MPU9150_ACC_GYRO_ADDR, MPU9150_ACCEL_ZOUT_H, &tempHi))
		return 0;
	if (!MPUReadRegister(MPU9150_ACC_GYRO_ADDR, MPU9150_ACCEL_ZOUT_L, &tempLo))
		return 0;
	vect->z = (tempHi << 8) | tempLo;
	
	return OKAY;
}

uint8_t MPUReadRegister(uint8_t devId, uint8_t regId, uint8_t *data)
{
	TWIStart();
	// Ensure "A START condition has been transmitted"
	if (TWIGetStatus() != 0x08)
		return ERROR;
	// Select device
	TWIWrite((devId << 1) | WRITE);
	// Ensure "SLA+W has been transmitted; ACK has been received"
	if (TWIGetStatus() != 0x18)
		return ERROR;
	// Send the register to query
	TWIWrite(regId);
	// Ensure "Data byte has been transmitted; ACK has been received"
	if (TWIGetStatus() != 0x28)
		return ERROR;
	// Send start, again
	TWIStart();
	// Ensure that "A repeated START condition has been transmitted"
	if (TWIGetStatus() != 0x10)
		return ERROR;
	// Select device
	TWIWrite((devId << 1) | READ);
	// Ensure "SLA+R has been transmitted; ACK has been received"
	if (TWIGetStatus() != 0x40)
		return ERROR;
	*data = TWIReadNack();
	if (TWIGetStatus() != 0x58)
		return ERROR;
	TWIStop();
	return OKAY;
}

uint8_t MPUWriteRegister(uint8_t devId, uint8_t regId, uint8_t data)
{
	TWIStart();
	// Ensure "A START condition has been transmitted"
	if (TWIGetStatus() != 0x08)
		return ERROR;
	// Select device
	TWIWrite((devId << 1) | WRITE);
	// Ensure "SLA+W has been transmitted; ACK has been received"
	if (TWIGetStatus() != 0x18)
		return ERROR;
	// Send the register to write to
	TWIWrite(regId);
	// Ensure "Data byte has been transmitted; ACK has been received"
	if (TWIGetStatus() != 0x28)
		return ERROR;
	// Write byte to device
	TWIWrite(data);
	// Ensure "Data byte has been transmitted; ACK has been received"
	if (TWIGetStatus() != 0x28)
		return ERROR;
	TWIStop();
	return OKAY;
}

uint8_t MPUGetGyroBias(uint16_t nReadings, vector3 *bias)
{
	/* GetGyroBias(...)
	 * The function takes as input a number of readings and a reference to a vector3 
	 * structure. The function takes the specified number of gyroscope readings and 
	 * averages them, applying the result to the supplied vector. The function returns 
	 * its status: OKAY or ERROR.
	 * 
	 * Note: It is critical that the device is stationary during calibration.
	 *
	 * TGT
	 */
	vector3 sum = {0};
	vector3 gyroData;
	
	uint16_t readingCount = 0;
	
	for(readingCount = 0; readingCount < nReadings; readingCount++)	{
		// Get the data from the device
		if (!MPUGetGyro(&gyroData))
			return ERROR;	
		// Apply reading to sum for each axis
		sum = v3_add(sum, gyroData);
	}
	// Average each axis by number of readings
	*bias = v3_scale(sum, nReadings, VECT_DIV);
	return OKAY;
}