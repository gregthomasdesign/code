// Strategy.h

#ifndef	STRATEGY_H
#define	STRATEGY_H

#include "Control.h"

/* STRATEGY OVERVIEW
 *
 * PLAY A:				PLAY B:				PLAY C:
 * 1) Easy money		1) Easy money		1) Easy money
 * 2) Narrows			2) Narrows			2) Narrows
 * 3) Flag (hold off)	3) Flag (blitz)		3) Cluster
 *						4) Cluster			4) Magma coin
 *						5) Magma coin		5) Flag (hail mary)
 */

control_segment pathA[] =
{
	{STRAIGHT,		15,		0},	// Exit starting circle 
	{TURN_RIGHT,	13,		0},	// Line up with easy money
	{STRAIGHT,		195,	0},	// Easy money
	{TURN_RIGHT,	13,		0},	// Head to narrows
	{STRAIGHT,		240,	0},	// Narrows
	{TURN_LEFT,		13,		0},	// Head to flag
	{STRAIGHT,		50,		0},	// Approach flag
	{DELAY,			0,		60000},	// Wait for 60 seconds	
	{STRAIGHT,		70,		6000},	// Get flag (with timeout to stop motors)
	{END,			0,		0}, 
};

control_segment pathB[] =
{
	{STRAIGHT,		15,		0},	// Exit starting circle
	{TURN_RIGHT,	13,		0},	// Line up with easy money
	{STRAIGHT,		190,	0},	// Easy money
	{TURN_RIGHT,	13,		0},	// Head to narrows
	{STRAIGHT,		244,	0},	// Narrows
	{TURN_LEFT,		13,		0},	// Head to flag
	{STRAIGHT,		120,	6000},	// Approach flag
	{RESET,			0,		0},	// Reset angle in case of wheel spin
	{TURN_RIGHT_BACK,20,	5000},	// Turn back to cluster
	{STRAIGHT,		300,	0},	// Head to cluster approach
	{TURN_RIGHT,	18,		0},	// Turn to cluster
	{STRAIGHT,		200,	0},	// Enter cluster
	{TURN_LEFT,		13,		0},	// Enter cluster
	{STRAIGHT,		14,		0},	// Double back
	{TURN_LEFT,		13,		0},	// Return to cluster
	{STRAIGHT,		400,	15000},	// Double back
	{DELAY,			0,		30000},
	{END,			0,		0},
};

control_segment pathC[] = {
	{STRAIGHT,		15,		0},	// Approach Easy Money
	{TURN_RIGHT,	13,		0},
	{STRAIGHT,		190,	0},	// Easy Money
	{TURN_RIGHT,	13,		0},
	{STRAIGHT,		350,	0},	// Narrows
	{TURN_RIGHT,	12,		0},
	{STRAIGHT,		135,	0},	// Approach cluster
	{TURN_RIGHT,	13,		0},
	{STRAIGHT,		135,	0},	// Enter cluster
	{TURN_LEFT,		13,		0},	// Double back
	{STRAIGHT,		13,		0},	// Double back
	{TURN_LEFT,		13,		0},	// Return to cluster
	{STRAIGHT,		400,	10000},	// Head for magma coin
	{RESET,			0,		0},	// Reset angle in case of wheel spin
	{TURN_LEFT_BACK,19,		5000},	// Aim for flag
	{STRAIGHT,		295,	25000}, // Attack flag
	{TURN_RIGHT,	5,		6000}, // Attack flag
	{STRAIGHT,		120,	10000}, // Attack flag
	{DELAY,			0,		30000},
	{END,			0,		0}
};

// Test paths
control_segment pathTurnTest[] = {
	{TURN_RIGHT,	104,	0},
	{END,			0,		0},
};

control_segment pathTimeoutTest[] = {
	{STRAIGHT,		500,	5000},
	{END,			0,		0},
};

control_segment pathDelayTest[] = {
	{STRAIGHT,		50,		0},
	{DELAY,			0,		10000},
	{STRAIGHT,		50,		0},
	{END,			0,		0},
};

control_segment pathTurnBackTest[] = {
	{TURN_LEFT_BACK,  26,	0},
	{STRAIGHT,        60,	0},
	{TURN_RIGHT_BACK, 13,	0},
	{TURN_RIGHT,      13,	0},
	{STRAIGHT,        60,	0},
	{TURN_LEFT_BACK,  20,	0},
	{STRAIGHT,        50,	0},
	{END,			  0,	0}
};

#endif // STRATEGY_H