
#include <avr/io.h>

#include "I2C.h"

/* TWI Interface */
// Initialize connection
void TWIInit(uint32_t scl_clock, uint32_t cpu_clock)
{
	//set SCL clock registers
	TWSR = 0x00;
	TWBR = ((cpu_clock/scl_clock)-16)/2;
	//enable TWI
	TWCR = (1<<TWEN);
}

// Generate start signal
void TWIStart(void)
{
	TWCR = (1<<TWINT)|(1<<TWSTA)|(1<<TWEN);
	// Wait until start condition is executed
	while ((TWCR & (1<<TWINT)) == 0);
}

// Generate stop signal
void TWIStop(void)
{
	TWCR = (1<<TWINT)|(1<<TWSTO)|(1<<TWEN);
	// Wait until stop condition is executed and bus released
	while(TWCR & (1<<TWSTO));
}

// Write byte of data
void TWIWrite(uint8_t u8data)
{
	TWDR = u8data;
	TWCR = (1<<TWINT)|(1<<TWEN);
	while ((TWCR & (1<<TWINT)) == 0);
}

// Read byte of data and acknowledge
uint8_t TWIReadAck(void)
{
	TWCR = (1<<TWINT)|(1<<TWEN)|(1<<TWEA);
	while ((TWCR & (1<<TWINT)) == 0);
	return TWDR;
}

// Read byte of data without acknowledge
uint8_t TWIReadNack(void)
{
	TWCR = (1<<TWINT)|(1<<TWEN);
	while ((TWCR & (1<<TWINT)) == 0);
	return TWDR;
}

// Read status
uint8_t TWIGetStatus(void)
{
	uint8_t status;
	//mask status
	status = TWSR & 0xF8;
	return status;
}
