// MetalDetector.c
// Implementation of the metal detector module
// Uses Timer2 to generate square wave

#include "MetalDetector.h"

#include <avr/io.h>
#include <avr/interrupt.h>

// Pins used for metal detector
#define metalPort		PORTD
#define metalPin		PIND
#define metalDDR		DDRD
#define metalOutputPin	(1 << 7)
#define metalInputPin	(1 << 6)

// Clock counter value
// Goal is 63.6 kHz
// = 14.7456 MHz / 8 (prescale) / (29) (counter)
#define metalClockCnt	29

ISR(TIMER2_COMPA_vect)
{
	// Toggle the output
	metalPort ^= metalOutputPin;
}

void metal_init()
{
	// Set up output pin
	metalDDR |= metalOutputPin;
	
	// Set up timer (but don't turn on yet)
	TCCR2A = (0 << COM2A1) | (0 << COM2A0)
	       | (0 << COM2B1) | (0 << COM2B0)
		   | (1 << WGM21) | (0 << WGM20);
		   
	TCCR2B = (0 << FOC2A) | (0 << FOC2B)
	       | (0 << WGM22) | (0 << CS22)
		   | (1 << CS21) | (0 << CS20);
		   
	OCR2A  = metalClockCnt - 1;
	
	TIMSK2 = (0 << OCIE2B) | (1 << OCIE2A)
	       | (0 << TOIE2);
}

void metal_enable()
{
	// Turn on timer
	TCCR2A |= (1 << CS21);
}


void metal_disable()
{
	// Turn off timer
	TCCR2A &= ~(1 << CS21);
}

uint8_t metal_detect()
{
	// Read input pin (turn into 1 or 0)
	return !!(metalPin & metalInputPin);
}
