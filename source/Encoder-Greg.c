#include "Encoder-Greg.h"

#include <util/atomic.h>
#include <stdio.h>
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>

int32_t leftTicks;
int32_t rightTicks;

uint8_t leftPin  = 0;
uint8_t rightPin = 0;

void encoder_setup(void)
{
	PCICR = (1 << PCIE3);
	PCMSK3 = (1 << PCINT27) | (1 << PCINT29);
	encoder_reset();
}

void encoder_reset(void)
{
	leftTicks = 0;
	rightTicks = 0;
}


int32_t encoder_getLeft()
{
	int32_t temp;
	ATOMIC_BLOCK(ATOMIC_FORCEON) {
		temp = leftTicks;
	}
	return temp;
}

int32_t encoder_getRight()
{
	int32_t temp;
	ATOMIC_BLOCK(ATOMIC_FORCEON) {
		temp = rightTicks;
	}
	return temp;
}

void encoder_get(int32_t* leftPtr, int32_t* rightPtr)
{
	ATOMIC_BLOCK(ATOMIC_FORCEON)
	{
		*leftPtr = leftTicks;
		*rightPtr = rightTicks;
	}
}

ISR(PCINT3_vect)
{
	uint8_t leftNew  = (PIND & (1 << PD4));
	uint8_t rightNew = (PIND & (1 << PD2));
	
	ATOMIC_BLOCK(ATOMIC_FORCEON)
	{
		leftTicks  += ( leftNew !=  leftPin);
		rightTicks += (rightNew != rightPin);
	}
	
	leftPin  =  leftNew;
	rightPin = rightNew;
	
	printf("%d %d\n", leftPin, rightPin);
}
