% define simulation time step
dt = 0.01;
% define simulation duration
t_max = 120;
% define simulation time vector
t = 0:dt:t_max;
% define desired outcome
% in the end, we want to control speed and direction
ctrl_speed = ones(size(t));
ctrl_dir = zeros(size(t));


