% ---------------------------- FILE INFO ----------------------------- %
% NAME:		randnorm.m
% AUTHOR: 	Thomas Gwynne-Timothy
% DATE:		02/25/2015
%
% Generates a random value from a normal distribution with a given mean
% and standard deviation. Generates one value at a time (does not work
% on vectors or matrices as input).
% -------------------------------------------------------------------- %
function [rn] = UCOM_randnorm(mean, dev)
if size(mean) == size(dev)
    rn = dev.*randn(size(mean)) + mean;
else
    error('The sizes of the input arguments must match')
end
end