close all
clear all

t = 0:0.01:50;
T = 10;

square = @(t,T) double(mod(t,T) > 0.5*T);

clean = UCOM_awgn(5*square(t,T), 30);
noise = UCOM_awgn(clean, 0);

figure
plot(t, clean)
xlabel('time')
ylabel('voltage')
grid on

figure
plot(t, noise)
xlabel('time')
ylabel('voltage')
grid on