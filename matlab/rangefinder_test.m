close all;

%% Analyze the distance measurements
clear all;
% load data from csv
raw_data = csvread('rangefinder_distance_test.csv');

% save data
distance_actual_ft = raw_data(:,1);
distance_measured_cm = raw_data(:,2);

% convert feet to inches
distance_actual_cm = distance_actual_ft * 30.48;

% plot measured vs actual
figure
plot(distance_actual_cm, distance_measured_cm)

% get scale and offset
p = polyfit(distance_actual_cm, distance_measured_cm, 1);
scale = p(1)    % get the slope
offset = p(2)   % get the y-intercept


%% Analyze the angle measurements
clear all;
% load data from csv
raw_data = csvread('rangefinder_angle_test.csv');

% save the data
angle_deg = raw_data(:,1);
angle_rad = angle_deg * pi / 180;
distance_measured_cm = raw_data(:,2);
distance_actual_cm = ones(size(distance_measured_cm))*30.48;

% plot the data
figure
polar(angle_rad, distance_measured_cm)
hold on
polar(angle_rad, distance_actual_cm)


              
                  