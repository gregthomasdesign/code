close all;
clear all;
squarewave = @(t, f) double(mod(t, (1/f)) > (1/f)/2);

t = 0:0.01:10;
f = 1;

ideal = squarewave(t, f);

fin = f*[0.1, 1, 1.2, 2, 4, 10];
input = [UCOM_awgn(squarewave(t, fin(1)), 30);
         UCOM_awgn(squarewave(t, fin(2)), 30);
         UCOM_awgn(squarewave(t, fin(3)), 30);
         UCOM_awgn(squarewave(t, fin(4)), 30);
         UCOM_awgn(squarewave(t, fin(5)), 30);
         UCOM_awgn(squarewave(t, fin(6)), 30)];

corr = zeros(size(input));
goertzels = zeros(1,size(corr,1));

for n = 1:size(corr, 1)
    corr(n,:) = conv(ideal, input(n,:), 'same');
    goertzels(n) = UCOM_goertzel(input(n,:), 100, f);
end

figure
plot(t, [ideal; input]);

figure
plot(t, corr)
legend('0.1','1','1.2','2','4','10')

goertzels

