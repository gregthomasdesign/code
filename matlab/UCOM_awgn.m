% ---------------------------- FILE INFO ----------------------------- %
% NAME:		UCOM_awgn.m
% AUTHOR: 	Thomas Gwynne-Timothy
% DATE:		02/26/2015
%
% Adds white gaussian noise to a supplied signal at a given SNR, in dB.
% Returns the noisy signal. If you just want noise, send a signal of zeros
% as input.
%
% -------------------------------------------------------------------- %
function [noisy_signal] = UCOM_awgn (signal, SNR_dB)
% Get SNR out of logarithmic scale
SNR = 10^(SNR_dB/10);
% Get power of noise to be added
noise_dev = sqrt(1/SNR);
% Generate noisy signal
noisy_signal = UCOM_randnorm(signal, noise_dev*ones(size(signal)));
end
