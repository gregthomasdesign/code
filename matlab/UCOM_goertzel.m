%% FILE INFO
% UCOM_goertzel.m
% Returns the normalized DFT magnitudes for a set of target frequencies.
% Uses a Goertzel algorithm.

function magnitude = UCOM_goertzel(signal, f_sample, targets)
% Get signal length
N = length(signal);
% Loop through each target frequency
for i = [1:length(targets)]
    f_target = targets(i);
    k = floor(N*f_target/f_sample + 0.5);
    w = 2.0*pi*k/N;
    cosine = cos(w);
    sine = sin(w);
    coeff = 2.0*cosine;

    d0 = 0.0;
    d1 = 0.0;
    d2 = 0.0;

    for n = 1:N
        d0  = signal(n) + coeff*d1 - d2;
        d2 = d1;
        d1 = d0;
    end

    scale_factor = N/2;
    real = (d1 - d2*cosine)/scale_factor;
    imag = (d2*sine)/scale_factor;
    magnitude(i) = abs(real + 1i*imag);
end
end