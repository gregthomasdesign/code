/*
 * Test_Encoder.c
 *
 * Created: 2015-07-17 10:00:36 PM
 *  Author: Greg
 */ 

#define F_CPU	14745600UL

#include <avr/io.h>
#include <avr/pgmspace.h>
#include <stdio.h>
#include <stdlib.h>
#include <util/delay.h>

#include "Encoder.h"
#include "Motor.h"

/* SERIAL OUTPUT DECLARATIONS & DEFINITIONS */
static int uart_putchar(char c, FILE *stream);
static int uart_getchar(FILE *stream);

FILE mystdout = FDEV_SETUP_STREAM(uart_putchar, NULL, _FDEV_SETUP_WRITE);
FILE mystdin = FDEV_SETUP_STREAM(NULL, uart_getchar, _FDEV_SETUP_READ);

static int uart_putchar(char c, FILE *stream)
{
	loop_until_bit_is_set(UCSR0A, UDRE0);
	UDR0 = c;
	return 0;
}

static int uart_getchar(FILE *stream)
{
	loop_until_bit_is_set(UCSR0A, RXC0); /* Wait until data exists. */
	return UDR0;
}

void init_uart(void)
{
	UCSR0B = (1<<RXEN0) | (1<<TXEN0);
	UBRR0 = 7;
	stdout = &mystdout;
	stdin = &mystdin;
}


int main(void)
{
	encoder_setup();
	motor_init();
	init_uart();
	
	motor_setFrequency(PSCL_256);
	
	sei();
	
	
	motor_setDirection(LEFT,  CW);
	motor_setDirection(RIGHT, CW);
	motor_setDutyCycle(LEFT,  0.25);
	motor_setDutyCycle(RIGHT, 0.25);
	
	
	motor_enable();
	
    while(1)
    {
		printf("%d %d\n", encoder_getLeft(), encoder_getRight());
		_delay_ms(200);
    }
}