// The path that the robot follows
 
//	Path A			12	
//	1) Easy Money	4
//	2) Narrows		3
//	3) Flag			5 (44 seconds)
control_segment pathA[] =
{
	{STRAIGHT,   205},
	{TURN_RIGHT, 13},
	{STRAIGHT,   240},
	{TURN_LEFT,  13},
	{STRAIGHT,   120},
	{END, 0},
};

//	Path B
//	1) Easy Money	4
//	2) Clusterfuck
//	3) Flag
control_segment pathB[] =
{
	{STRAIGHT,   205},
	{TURN_RIGHT,  13},
	{STRAIGHT,    40},
	{TURN_RIGHT,   7},
	{STRAIGHT,   240},
	{TURN_LEFT,    7},
	{STRAIGHT,    40},
	{TURN_LEFT,   13},
	{STRAIGHT,   300},
	{END, 0},
};


//	Path C
//	1) Easy Money
//	2) Narrows		
//	3) Pipe
//	4) Flag
control_segment pathC[] = {
	{STRAIGHT,   205},	// Easy Money
	{TURN_RIGHT,  13},
	{STRAIGHT,   270},	// Narrows
	{TURN_RIGHT,  13},
	{STRAIGHT,    70},	// Approach pipe/cluster
	{TURN_LEFT,   13},
	{STRAIGHT,   105},	// Approach/enter pipe
	{TURN_LEFT,   13},
	{STRAIGHT,   110},	// Through pipe
	{TURN_LEFT,   13},
	{STRAIGHT,    90},	// Out pipe
	{TURN_RIGHT,   7},
	{STRAIGHT,    90},	// Approach flag
	{END, 0},
};

//	Path DeezNutz
//	1) Easy Money
//	2) Narrows
//	3) ClusterF%#@
//	4) Lone Ranger
control_segment pathDeezNutz[] = {
	{STRAIGHT,   205},	// Easy Money
	{TURN_RIGHT,  13},
	{STRAIGHT,   350},	// Narrows
	{TURN_RIGHT,  12},
	{STRAIGHT,   140},	// Approach cluster
	{TURN_RIGHT,  13},
	{STRAIGHT,   135},	// Enter cluster
	{TURN_LEFT,   13},	// Enter cluster
	{STRAIGHT,    15},	// Double back
	{TURN_LEFT,   12},	// Return to cluster
	{STRAIGHT,   250},	// Double back
	{END, 0},
};