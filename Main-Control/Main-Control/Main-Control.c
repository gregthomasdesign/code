/*
 * Main_Control.c
 *
 * Created: 2015-07-17 11:28:03 PM
 *  Author: Greg
 */ 

#define F_CPU	14745600UL

#include <util/delay.h>

#include "Motor.h"
#include "Encoder.h"
#include "Control.h"
#include "Status.h"
#include "Strategy.h"

// UART stuff
#include <stdio.h>
#include <stdlib.h>

/* SERIAL OUTPUT DECLARATIONS & DEFINITIONS */
static int uart_putchar(char c, FILE *stream);
static int uart_getchar(FILE *stream);

FILE mystdout = FDEV_SETUP_STREAM(uart_putchar, NULL, _FDEV_SETUP_WRITE);
FILE mystdin = FDEV_SETUP_STREAM(NULL, uart_getchar, _FDEV_SETUP_READ);

static int uart_putchar(char c, FILE *stream)
{
	loop_until_bit_is_set(UCSR0A, UDRE0);
	UDR0 = c;
	return 0;
}

static int uart_getchar(FILE *stream)
{
	loop_until_bit_is_set(UCSR0A, RXC0); /* Wait until data exists. */
	return UDR0;
}

void init_uart(void)
{
	UCSR0B = (1<<RXEN0) | (1<<TXEN0);
	UBRR0 = 7;
	stdout = &mystdout;
	stdin = &mystdin;
}
// End of UART


// Mainline
int main(void)
{
	// Wait before starting...
	_delay_ms(1000);
	
	// Set up modules
	init_uart();
	
	motor_init();
	motor_setFrequency(PSCL_8);
	
	encoder_setup();
	LED_init();
	
	control_setupPath(pathB);
	
	
	sei();
	
	// Modules are all set up
	printf("Setup complete.\n");
	
	printf("Running control system...\n");
	control_run();
	
	// Done -- stop and do nothing
    while(1)
    {
    }
}