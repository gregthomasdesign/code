// test-motor.c
// A mainline to test the motor driver PWM

#include <avr/interrupt.h>
#include "Motor.h"

int main(void)
{
	// Turn on interrupts
	sei();

	// Set up motor
	motor_init();
	motor_setDutyCycle(LEFT,  0.3);
	motor_setDutyCycle(RIGHT, 0.3);
	motor_setDirection(LEFT,  CW);
	motor_setDirection(RIGHT, CW);
	motor_setFrequency(PSCL_256);
	motor_enable();
	
	// Let the interrupts run on their own
    while(1)
    {
    }
}