/*
 * Test_MetalDetector.c
 *
 * Created: 2015-07-12 12:56:19 PM
 *  Author: Greg
 */ 

#define F_CPU 14745600UL

#include <util/delay.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include "MetalDetector.h"

//---- Things for printing to PUTTY --------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <avr/pgmspace.h>

static int uart_putchar(char c, FILE *stream);
static int uart_getchar(FILE *stream);

FILE mystdout = FDEV_SETUP_STREAM(uart_putchar, NULL, _FDEV_SETUP_WRITE);
FILE mystdin = FDEV_SETUP_STREAM(NULL, uart_getchar, _FDEV_SETUP_READ);

static int uart_putchar(char c, FILE *stream)
{
	loop_until_bit_is_set(UCSR0A, UDRE0);
	UDR0 = c;
	return 0;
}

static int uart_getchar(FILE *stream)
{
	loop_until_bit_is_set(UCSR0A, RXC0); /* Wait until data exists. */
	return UDR0;
}

void init_uart(void)
{
	UCSR0B = (1<<RXEN0) | (1<<TXEN0);
	UBRR0 = 7;
	stdout = &mystdout;
	stdin = &mystdin;
}


int main(void)
{
	init_uart();
	printf("Test\n");
	// Set up and turn on metal detector
	metal_init();
	sei();
	
	metal_enable();
    
	// Check for metal
	while(1)
	{
		if(metal_detect())
		{
			printf("Metal!\n");
		}
		else
		{
			printf("No metal...\n");
		}
		
		_delay_ms(100);
	}
}