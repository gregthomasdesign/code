// A self-contained testing setup for Goertzel's algorithm
// gcc -std=c99 -o main.exe main.c

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#define PI			3.1415926536
#define SIG_FREQ	500.
#define NUM_PTS		100

// Goertzel variables
double d[3];
double coeff;


// Generate data
int16_t getSignal(double time)
{
	printf("%d ", (int)(time*1000000));
	double phase = 2*PI*SIG_FREQ*time;
	return 512 + 256*cos(phase);
}

// Take one step in the Goertzel algorithm
void processData(int16_t data)
{
	d[0] = data + d[1]*coeff - d[2];
	d[2] = d[1];
	d[1] = d[0];
	
	printf("%d %d %d\n", data, (int)d[1], (int)d[2]);
}

// Turn the state variables into magnitudes
uint32_t getMagnitude()
{
	return sqrt(d[1]*d[1] + d[2]*d[2] - d[1]*d[2]*coeff);
}

// Apply the Goertzel algorithm to some data
uint16_t calculateGoertzel(double chk_freq, double samp_freq)
{
	// Set up sampling times
	double dt = 1 / samp_freq;

	// Initialize state table
	d[0] = 0;
	d[1] = 0;
	d[2] = 0;

	// Initialize Goertzel coefficient
	int k = (chk_freq / samp_freq) * NUM_PTS + 0.5;
	coeff = 2 * cos(2*PI*k/NUM_PTS);
	
	// Process samples
	for(int i = 0; i < NUM_PTS; i++)
	{
		double t = dt * i;
		int16_t data = getSignal(t);
		
		processData(data);
	}
	
	// Get magnitude and divide by scaling factor
	return getMagnitude() / (NUM_PTS / 2);
}

// Mainline
int main(void)
{
	// The sampling frequency
	// Choose between timer mode (nice round frequencies) or free-running mode
	double f = (14745600 / 128 / 13);
	//double f = 4000;
	
	// Input loop
	double chk_freq = 1;
	while(chk_freq > 0.1)
	{
		scanf("%lf", &chk_freq);
		printf("%f %d\n", chk_freq, calculateGoertzel(chk_freq, f));
		printf("%f %f\n", d[1], d[2]);
	}

	return 0;
}