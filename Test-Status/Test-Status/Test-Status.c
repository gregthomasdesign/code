/*
 * Test_Status.c
 *
 * Created: 2015-07-21 5:49:56 PM
 *  Author: Greg
 */ 

#define F_CPU 14745600UL

#include <avr/io.h>
#include <util/delay.h>

#include "Status.h"

int main(void)
{
	LED_init();
	
	int x = 0;
    while(1)
    {
		if(x % 2)
			LED_set(LED_BLUE);
		else
			LED_clear(LED_BLUE);
			
		if(x / 2)
			LED_set(LED_RED);
		else
			LED_clear(LED_RED);
			
		_delay_ms(250);
		
		x = (x + 1) % 4;
    }
}