# README #

This is the full code repository for Group 7's project (The Blue Shell) in the 2015 ECED3901 design course.

### What's in this repository? ###

This repository contains all of the code that was written for this course, including:

* Stand-alone modules for the robot's hardware and integration, in /source
* The final program that was used on the robot, in /Main-Control
* A series of small testing programs that were used throughout the project's development, in /Test-XYZ, where XYZ is the name of the module under test
* Some Matlab scripts for simulating the light detector, plotting the rangefinder's data, and for testing the PID controller in Simulink, in /matlab
* A few other whimsies that aren't central to the project

### Questions? ###

If you have any questions, please don't hesitate to contact one of us at

* greg.deon@dal.ca
* thomas.gt@dal.ca